import random
import math
import numpy as np

#Calcul de la norme euclidienne
def norme(X):
    return np.linalg.norm(X, 2)

#Calcul de la plus grande valeur propre et de son vecteur propre associé pour une matrice
def puiss_iter(mat, epsilon=0.000001):
    m = np.shape(mat)[0]                            #Nombre de lignes de la matrice
    X = np.matrix([[1.0] for i in range(m)])        #Initialise un vecteur colonne de 1.0 de taille m
    val_anc, valp = 1, 0                            #Initialisation valeurs propres
    while (abs(valp - val_anc) > epsilon):          #Condition de convergence avec epsilon comme approximation
        val_anc = valp
        vecb    = X / norme(X)
        X       = mat*vecb
        valp    = (np.transpose(vecb)*X).item(0)
    return (valp,vecb)

#Calcul des autres valeurs propres et vecteurs propres associées par itération à partir de la plus grande valeur propre
def deflation(A):
    compt     = 1                                   #Initialisation d'un compteur
    A         = np.matrix(A)                        #Transformation d'une matrice (liste de liste) en matrice numpy (np.matrix) pour faciliter les calculs matriciels
    it_max    = np.shape(A)[0]                      #Nombre d'itération max = nombre de variables
    resultat  = puiss_iter(A)                       #Récupère la plus grande valeur propre et son vecteur propre associé
    valp, u_n = resultat[0], resultat[1]            #Affectation de la plus grande valeur propre et vecteur propre associé
    valeurs_propres = [valp]                        #Création d'une liste des valeurs propres
    vects_propres   = u_n                           #Création d'une matrice qui contiendra les vecteurs propres
    while(compt != it_max):
        v_n = u_n / (np.transpose(u_n) * u_n)       
        A   = A - valp * u_n * np.transpose(v_n)    

        #Application de la méthode des puissances itérées sur la nouvelle matrice
        resultat = puiss_iter(A)
        valp     = resultat[0]
        u_n      = resultat[1]

        #Stockage des valeurs propres dans la liste
        valeurs_propres.append(valp)

        #Stockage des vecteurs propres en une seule matrice
        vects_propres = np.hstack((vects_propres, u_n))
        compt += 1
    return (valeurs_propres,vects_propres)