# coding: utf-8

from init import *
from calculs_unitaire import *
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D, proj3d
from matplotlib.patches import FancyArrowPatch

#Définition d'une classe pour les vecteurs en 3D
class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

#Récupération des coordonnées de chaque individu sur chaque axe
def coord_indiv(donnees_res):
    coord = [[] for k in range(nbVar)]
    for j in range(nbVar):
        for i in range(nbIndividus):
            coord[j].append(donnees_res[i][j]) #Coordonnées des individus i sur l'axe j

    return coord

#Représenation graphique des individus
def repres_indiv(donnees_res, indiv_noms, nb_dim, liste_pct):
    coord = coord_indiv(donnees_res)

    #Teste le nombre de dimensions voulues (si 3 -> graphique 3D sinon graphique(s) en 2D)
    if (nb_dim == 3):
        fig = plt.figure() #Crée une variable pour une fenêtre graphique
        
        #Paramètre de la fenêtre graphique
        rep3d = fig.add_subplot(111, projection="3d", title="Coordonnées des individus dans le nouveau repère", xlabel="Dim 1 ("+str(liste_pct[0])+"%)", ylabel="Dim 2 ("+str(liste_pct[1])+"%)", zlabel="Dim 3 ("+str(liste_pct[2])+"%)")
        
        rep3d.scatter(coord[0], coord[1], coord[2], zdir="z", marker="o", s=50) #Trace les individus sur le graphique
        
        #Légende les points avec le nom des individus
        for label, i, j, k in zip(indiv_noms, coord[0], coord[1], coord[2]):
            rep3d.text(s=label, x=i, y=j, z=k)
    else:
        fig = [plt.figure() for k in range(nb_dim-1)] #On ne prend qu'un nombre (nb_dim-1) de graph car on ne représente pas le graph de la 1er composante avec elle-même
        
        for k in range(nb_dim-1):
            #Détermine les bornes du graphiques
            lim_x = max(abs(min(coord[0])), abs(max(coord[0])))
            lim_y = max(abs(min(coord[k+1])), abs(max(coord[k+1])))
            
            #Paramètre des fenêtres graphiques
            rep2d = fig[k].add_subplot(111, title="Coordonnées des individus dans le nouveau repère", xlabel="Dim 1 ("+str(liste_pct[0])+"%)", ylabel="Dim "+str(k+2)+" ("+str(liste_pct[k+1])+"%)", xlim=[-lim_x*1.05, lim_x*1.05], ylim=[-lim_y*1.1, lim_y*1.1])
            
            rep2d.scatter(coord[0], coord[k+1], marker="o", s=50) #Trace les individus sur le graphique
            
            #Légende les points avec le nom des individus
            for label, i, j in zip(indiv_noms, coord[0], coord[k+1]):
                rep2d.annotate(s=label, xy=(i,j), xytext=(-10,10), textcoords="offset points")
            
            #Place les axes sur la figure    
            ax = fig[k].gca()
            ax.spines["right"].set_position(("data", 0))
            ax.spines["top"].set_position(("data", 0))
            ax.xaxis.set_ticks_position("top")
            ax.yaxis.set_ticks_position("right")
            rep2d.grid()

    return fig

#Récupération des variables de départ et variables du nouveau repère sous forme de listes dans des listes
def coord_corr(donnees_res):
    listxj, listfk = [], []
    for i in range(nbVar):
        xj, fk = zeros(1, nbIndividus), zeros(1, nbIndividus)
        for j in range(nbIndividus):
            xj[j][0] = donnees[j][i]     #Variables de départ
            fk[j][0] = donnees_res[j][i] #Variables dans le nouveau repère
        listxj.append(xj)
        listfk.append(fk)

    projv = [[] for k in range(nbVar)]
    for j in range(nbVar):
        for i in range(nbVar):
                projv[j].append(correlation(listxj[i], listfk[j])) #Coordonnées de corrélation des variables pour la composante j

    return projv

#Représentation graphique du cercle de corrélation
def cercle_corr(donnees_res, var_noms, nb_dim, liste_pct):
    projv  = coord_corr(donnees_res)
    
    #Teste le nombre de dimensions voulues (si 3 -> graphique 3D sinon graphique(s) en 2D)
    if (nb_dim == 3):
        fig = plt.figure() #Crée une variable pour une fenêtre graphique
        
        #Paramètre de la fenêtre graphique
        sphere = fig.add_subplot(111, projection="3d", title="Sphère de corrélation des variables avec les composantes", xlabel="Dim 1 ("+str(liste_pct[0])+"%)", ylabel="Dim 2 ("+str(liste_pct[1])+"%)", zlabel="Dim 3 ("+str(liste_pct[2])+"%)")
        
        sphere.scatter(projv[0], projv[1], projv[2], zdir="z", marker="o", s=50) #Trace les variables sur le graphique
        
        #Légende les points avec le nom des variables
        for label, i, j, k in zip(var_noms, projv[0], projv[1], projv[2]):
            sphere.text(s=label, x=i, y=j, z=k)
            a = Arrow3D([0,i],[0,j],[0,k], mutation_scale=20, lw=1, arrowstyle="-|>", color="k") #Vecteur partant de l'origine du repère jusqu'à la variable
            sphere.add_artist(a) #Trace le vecteur
        
        #Paramètres et tracé de la sphère    
        theta, phi = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
        x = np.sin(phi)*np.cos(theta)
        y = np.sin(phi)*np.sin(theta)
        z = np.cos(phi)
        sphere.plot_wireframe(x, y, z, color="g")
    else:
        fig = [plt.figure() for k in range(nb_dim-1)] #On ne prend qu'un nombre (nb_dim-1) de graph car on ne représente pas le graph de la 1er composante avec elle-même
        
        for k in range(nb_dim-1):
            #Paramètres des fenêtres graphiques
            circle = fig[k].add_subplot(111, title="Cercle de corrélation des variables avec les composantes", xlabel="Dim 1 ("+str(liste_pct[0])+"%)", ylabel="Dim "+str(k+2)+" ("+str(liste_pct[k+1])+"%)")
            
            circle.plot(projv[0], projv[k+1], 'ko') #Place les variables sur le graphe
            
            #Légende des points avec le nom des variables
            for label, i, j in zip(var_noms, projv[0], projv[k+1]):
                circle.annotate(s=label, xy=(i,j), xytext=(-10,10), textcoords="offset points")
                circle.arrow(0, 0, i, j, head_width=0.05, head_length=0.05, fc='k', ec='k') #Trace les vecteurs partant de l'origine du repère jusqu'à la variable

            #Paramètres du cercle
            theta = np.linspace(0, 2*np.pi, 100)
            c1    = np.cos(theta)
            c2    = np.sin(theta)

            #Trace le cercle et place les axes sur la figure
            ax = fig[k].gca()
            ax.spines["right"].set_position(("data", 0))
            ax.spines["top"].set_position(("data", 0))
            ax.xaxis.set_ticks_position("top")
            ax.yaxis.set_ticks_position("right")
            circle.plot(c1, c2, "k")
            circle.axis("equal")
            circle.grid()

    return fig

def graphe(donnees_res, nb_dim, liste_pct, indiv_noms, var_noms):

    fig_indiv = repres_indiv(donnees_res, indiv_noms, nb_dim, liste_pct)

    fig_cercle_corr = cercle_corr(donnees_res, var_noms, nb_dim, liste_pct)
    
    plt.show()

#Calcule le cos² des variables
def cos2_variables(donnees_res):
    cos2_var = coord_corr(donnees_res)
    for i in range(nbVar):
        for j in range(nbVar):
            cos2_var[i][j] = cos2_var[i][j]**2
    return cos2_var

#Calcule la contribution des variables aux axes
def contrib_var(donnees_res, liste_val_p):
    corr_var = coord_corr(donnees_res)
    for i in range(nbVar):
        for j in range(nbVar):
            corr_var[i][j] = ((corr_var[i][j]**2) / liste_val_p[i]) * 100
    return corr_var

#Calcule le cos² des individus
def cos2_individus(donnees_res):
    mat_contrib = zeros(nbVar, nbIndividus)
    for i in range(nbIndividus):
        dist_i = distance(donnees_res[i][:])
        for j in range(nbVar):
            mat_contrib[i][j] = (donnees_res[i][j] / dist_i)**2
    return mat_contrib

#Calcule la contribution d'un individu à un axe
def contrib_indiv(donnees_res, liste_val_p):
    mat_contrib = zeros(nbVar, nbIndividus)
    for i in range(nbIndividus):
        for j in range(nbVar):
            mat_contrib[i][j] = ((donnees_res[i][j]**2) / (nbIndividus * liste_val_p[j])) * 100
    return mat_contrib