# coding: utf-8

import xlrd #Module utile à l'exploitation de fichier xls
import csv
import os
import numpy as np

#Demande à l'utilisateur le nom du fichier à traiter
os.system("clear")                                                                  #Nettoie le terminal pour plus de clareté
fichier   = input("Nom du fichier à utiliser (fichier xls ou csv uniquement) : ")   #Demande le nom du fichier à traiter (avec extension)
extension = fichier[-3:]                                                            #Récupère l'extension du fichier

#Teste si le fichier existe dans le dossier courrant sinon quitte
if(os.path.isfile(fichier)):
    print("Fichier trouvé.")
else:
    print("Fichier non trouvé.")
    raise SystemExit

if (extension == "csv"):
    delim = ""
    while(delim == ""):
        print("Vérifiez votre délimiteur dans votre fichier csv puis saisissez le.")
        try:
            delim = input("Entrez le délimiteur de votre fichier : ")   #Récupère le délimiteur du fichier csv
        except ValueError:
            delim = ""
    fich          = open(fichier, "r")                                  #Ouvre le fichier csv en mode lecture
    reader        = csv.reader(fich, delimiter=delim)                   #Récupère les données du fichier csv dans un tableau
    donnees_brute = []                                                  #Variable pour accueillir les données du fichier csv sous forme de liste de liste
    for ligne in reader:
        donnees_brute.append(ligne)                                     #Récupère ligne par ligne les données du fichier csv
        
        nbVar       = len(donnees_brute[0]) - 1
        nbIndividus = len(donnees_brute) - 1
else:
    #Importation de la feuille de travail
    wb          = xlrd.open_workbook(fichier)                           #Ouverture du fichier xls
    sheet_name  = wb.sheet_names()                                      #Récupère les feuilles du fichier dans un tableau
    table       = wb.sheet_by_name(sheet_name[0])                       #Choix de la feuille de travail
    
    nbVar       = table.ncols - 1                                       #Nombre de variables à gérer
    nbIndividus = table.nrows - 1                                       #Nombre d'individus dans notre echantillon

#Demande à l'utilisateur s'il souhaite un pourcentage par défaut ou quel pourcentage de restitution du nuage il souhaite
rep = input("Souhaitez-vous un pourcentage de restitution par défaut (80%) ? (o/n) ")
if(rep == "n"):
    pourcentage = -1
    while(pourcentage > 100 or pourcentage <= 0):
        print("Votre pourcentage doit être compris entre 0 et 100.")
        try:
            pourcentage = float(input("Pourcentage de restitution souhaité : "))
        except ValueError:
            pourcentage = -1
else:
    pourcentage = 80

donnees     = np.zeros((nbIndividus, nbVar))                            #Matrice de 0 de taille nbIndividus x nbVar

#Affiche le nombre de variables et d'individus
def info_init():
    print("Nombre de variables : ", nbVar)
    print("Nombre d'individus : ", nbIndividus,"\n")

#Récupère les données du tableau dans un dictionnaire python
def init_donnees():
    var_noms, indiv_noms = [], []
    if (extension == "xls"):
        for i in range(nbIndividus):
            indiv_noms.append(table.cell_value(i+1, 0))
            for j in range(nbVar):
                donnees[i][j] = float(table.cell_value(i+1, j+1))
                if (i == 0):
                    var_noms.append(table.cell_value(i, j+1))
    else:
        for i in range(nbIndividus):
            indiv_noms.append(donnees_brute[i+1][0])
            for j in range(nbVar):
                donnees[i][j] = float(donnees_brute[i+1][j+1])
                if (i == 0):
                    var_noms.append(donnees_brute[i][j+1])
    return {"donnees":donnees, "nbVar":nbVar, "nbIndiv":nbIndividus, "indiv_noms":indiv_noms, "var_noms":var_noms, "pourcentage":pourcentage}

#Affiche des matrices
def affichage_matrice(*matrice):
    for mat in matrice:
        for i in range(len(mat)):
            print(mat[i][:])
        print("\n")

#Initialise une matrice de 0 de taille nbcol x nbligne
def zeros(nbcol=0, nbligne=0):
    if(nbligne == 0):
        return [0 for i in range(nbcol)]
    else:
        return [[0 for i in range(nbcol)] for k in range(nbligne)]
