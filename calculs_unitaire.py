# coding: utf-8

import math
from init import *

#Calcule la moyenne
def moyenne(matrice, taille, numC):
    somme = 0
    for i in range(taille):
        somme += matrice[i][numC]
    return (somme / taille)

#Calcule la distance d'un point par rapport à l'origine
def distance(coord_indiv):
    dist = 0
    for i in coord_indiv:
        dist += i**2
    return math.sqrt(dist)

#Calcule l'écart-type
def ecart_type(matrice, numC):
    somme  = 0
    taille = len(matrice)
    m      = moyenne(matrice, taille, numC)
    for i in range(taille):
        somme += ((matrice[i][numC] - m)**2) / taille
    return math.sqrt(somme)

#Calcule le produit de covariance
def covariance(matrice, matrice2, X, Y):
    somme  = 0
    taille = len(matrice)
    m1     = moyenne(matrice, taille, X)
    m2     = moyenne(matrice, taille, Y)
    for i in range(taille):
        somme += (matrice[i][X] - m1) * (matrice2[i][Y] - m2)
    return (somme / taille)

#Calcule le produit de corrélation
def correlation(vec1, vec2, i=0, j=0):
    ecart1 = ecart_type(vec1, i)
    ecart2 = ecart_type(vec2, j)
    return covariance(vec1, vec2, i, j) / (ecart1*ecart2)

#Détermine le nombre de dimensions nécessaires pour obtenir un pourcentage de restitution supérieur strict au pourcentage souhaité par l'utilisateur
def dimensions(liste_val_p, pourcentage):
    nb_valp   = len(liste_val_p)
    liste_pct = [round((i / nb_valp) * 100, 3) for i in liste_val_p]
    print("Liste des pourcentages respectifs :", liste_pct)
    somme_vp = 0
    num_vp   = 0
    while(somme_vp < pourcentage):
        somme_vp += liste_pct[num_vp]
        num_vp   += 1
    print("\nPourcentage de restitution obtenu :", str(round(somme_vp, 3))+"%")
    return (num_vp, liste_pct)