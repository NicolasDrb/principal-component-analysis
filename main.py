# coding: utf-8

#Import des modules
import time
import sys
import numpy as np
from io import StringIO

#Import de nos fichiers
from init import *                   #Initialisation du programme et fonctions d'initialisation
from calculs_unitaire import *       #Calculs et formules numériques
from calculs_matriciel import *      #Calculs et opérations sur les matrices
from puissance_iteree import *       #Algorithme des puissances itérées
from graphique import *              #Fonctions graphiques

sauvegarde = sys.stdout              #Sauvegarde du comportement de la sortie standard (les résultats des print())
sortie     = StringIO()              #Créer un chaîne de caractère qui contiendra tout ce qui passe par la sortie standard
sys.stdout = sortie                  #La sortie standard est dirigée vers la variable sortie

temps_init = time.clock()            #Récupère l'heure actuelle pour calculer le temps d'exécution

#Récupération des données
info_init()                          #Affiche le nombre d'individus et de variables
dico         = init_donnees()        #Récupère le contenu du tableau .xls dans un dictionnaire python
donnees      = dico["donnees"]       #Contient les données de chaque individus
pourcentage  = dico["pourcentage"]   #Contient le pourcentage minimum de densité du nuage final
nbVar        = dico["nbVar"]         #Contient le nombre de variables
nbIndividus  = dico["nbIndiv"]       #Contient le nombre d'individus
indiv_noms   = dico["indiv_noms"]    #Contient les noms des individus
var_noms     = dico["var_noms"]      #Contient les noms des variables

#Initialisation des matrices
mat_type     = centree_type(donnees) #Retourne un dictionnaire avec les différentes matrices centrées
mat_reduite  = mat_type["reduite"]   #Matrice centrée réduite
mat_normee   = mat_type["normee"]    #Matrice centrée normée
mat_correlee = mat_cor(mat_normee)   #Calcul de la matrice de corrélation à partir de la matrice centrée normée

#Arrondissement à 2 décimales et affichage des matrices calculées précédemment
print("Matrice centrée réduite :"); affichage_matrice(np.around(mat_reduite, 2))
#print("Matrice centrée normée :"); affichage_matrice(np.around(mat_normee, 2))
print("Matrice de corrélation :"); affichage_matrice(np.around(mat_correlee, 2))

#Calcul des valeurs propres et vecteurs propres avec la méthode des puissances itérées
algo_valp = deflation(mat_correlee)
print("Valeurs propres :", [round(elem, 5) for elem in algo_valp[0]])
print("\nVecteurs propres :"); affichage_matrice(np.around(algo_valp[1], 5))

#Calcul des nouvelles coordonnées des individus dans notre nouvelle base
resultat_np_norm = mat_normee * algo_valp[1]
resultat_np_redu = mat_reduite * algo_valp[1]

#Changement du type de la matrice resultat_np (np.matrix -> liste de liste)
lignes  = resultat_np_norm.shape[0]
col     = resultat_np_norm.shape[1]
resultat_norm = zeros(col,lignes)
resultat_redu = zeros(col, lignes)
for i in range(lignes):
    for j in range(col):
        resultat_norm[i][j] = resultat_np_norm.item((i, j))
        resultat_redu[i][j] = resultat_np_redu.item((i, j))
        
print("Individus dans la nouvelle base (normée) :"); affichage_matrice(np.around(resultat_norm, 8))

print("Individus dans la nouvelle base (réduite) :"); affichage_matrice(np.around(resultat_redu, 8))

corr_var_comp = coord_corr(resultat_redu)
print("Corrélation des variables (colonnes) avec les composantes (lignes) :"); affichage_matrice(np.around(corr_var_comp, 5))

cos2_var = cos2_variables(resultat_redu)
print("Cos² des variables (colonnes) par rapport aux axes (lignes) :"); affichage_matrice(np.around(cos2_var, 5))

contribution_var = contrib_var(resultat_redu, algo_valp[0])
print("Contribution (en pourcentage) des variables (colonnes) aux axes (lignes) :"); affichage_matrice(np.around(contribution_var, 5))

cos2_indiv = cos2_individus(resultat_redu)
print("Cos² des individus (colonnes) par rapport aux axes (lignes) :"); affichage_matrice(np.around(cos2_indiv, 4))

contribution_indiv = contrib_indiv(resultat_redu, algo_valp[0])
print("Contribution (en pourcentage) des individus (lignes) aux axes (colonnes) :"); affichage_matrice(np.around(contribution_indiv, 5))

#Détermination du nombre de dimensions à utilser, ainsi que leur pourcentage respectif de restitution
nb_dim, liste_pct = dimensions(algo_valp[0], pourcentage)

#Affichage du temps d'exécution du programme
print("\nLe temps d'exécution est de "+str(round(time.clock() - temps_init, 4))+"s")

#Restauration du comportement de la sortie standard
sys.stdout = sauvegarde

#Récupération de la valeur de sortie sous forme de chaîne
result = sortie.getvalue()

rep = input("Voulez-vous récupérer les résultats dans un fichier texte ? (o/n) ")
if(rep == "o"):
    #Ecriture des résultats obtenus dans un fichier
    nom_fichier = input("Entrez le nom du fichier sans extension (par défaut appuyez sur "+"Entrée"+" : [nom_fichier_départ]_res) : ")
    if (nom_fichier != ""):
        fichier_res = open(nom_fichier+".txt", "w")
    else:
        fichier_res = open(fichier[:-4]+"_res.txt", "w")
    fichier_res.write(result)
    fichier_res.close()
else:
    print(result)
    
#Projection des individus sur le/les graphe(s)
graphe(resultat_redu, nb_dim, liste_pct, indiv_noms, var_noms)