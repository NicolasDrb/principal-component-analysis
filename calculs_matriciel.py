# coding: utf-8

from calculs_unitaire import *
from init import *

#Calcule la matrice de corrélation
def mat_cor(matrice):
    mat   = zeros(nbVar, nbVar)
    for i in range(nbVar):
        mat[i][i] = float(1)
    for i in range(nbVar):
        for j in range(i+1, nbVar):
            res1 = correlation(matrice, matrice, i, j)
            mat[i][j] = res1
            mat[j][i] = res1
    return mat

#Obtenir la matrice centrée-réduite et centrée-normée
def centree_type(matrice):
    mat_reduite = zeros(nbVar, nbIndividus)
    mat_normee = zeros(nbVar, nbIndividus)
    for j in range(nbVar):
        for i in range(nbIndividus):
            moy   = moyenne(matrice, nbIndividus, j)
            ecart = ecart_type(matrice, j)
            mat_reduite[i][j] = (matrice[i][j] - moy) / ecart
            mat_normee[i][j]  = (matrice[i][j] - moy) / (ecart*math.sqrt(nbIndividus))
    return {"reduite":mat_reduite, "normee":mat_normee}
    
#Supprime un individu d'une matrice à partir de son indice
def supprimer_ind(mat, index):
    matrice = zeros(len(mat[0]),len(mat)-1)
    for i in range(index):
        matrice[i][:] = mat[i][:]
    for i in range(index+1,len(mat)):
        matrice[i-1][:] = mat[i][:]
    return matrice